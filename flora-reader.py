#!/usr/bin/python3

import sys
import time
from miflora.miflora_poller import MiFloraPoller, MI_BATTERY, MI_CONDUCTIVITY, MI_LIGHT, MI_MOISTURE, MI_TEMPERATURE

MAC = "C4:7C:8D:62:AE:AE"

poller = MiFloraPoller(MAC)

# print("Temperature: {}".format(poller.parameter_value("temperature")))
print("FW: {}".format(poller.firmware_version()))
print("Name: {}".format(poller.name()))
print("Battery level: {}%".format(poller.parameter_value(MI_BATTERY)))
print("Conductivity: {}".format(poller.parameter_value(MI_CONDUCTIVITY)))
print("Light: {}lx".format(poller.parameter_value(MI_LIGHT)))
print("Moisture: {}%".format(poller.parameter_value(MI_MOISTURE)))
print("Temperature: {} C".format(poller.parameter_value(MI_TEMPERATURE)))